package things.dev

import androidx.test.ext.junit.runners.AndroidJUnit4
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import things.dev.features.wifiutils.framework.WifiRepository
import things.dev.features.wifiutils.framework.models.NetworkEvent
import things.dev.features.wifiutils.framework.models.WifiSecurity
import javax.inject.Inject

@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class WifiRepositoryTest {
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private val lanSsid = BuildConfig.WIFI_LAN_SSID
    private val lanBssids = BuildConfig.WIFI_LAN_BSSIDS.split(",")
    private val lanSecurity = WifiSecurity.WPA2
    private val lanPassword = BuildConfig.WIFI_LAN_PASS

    private val guestLanSsid = BuildConfig.WIFI_GUEST_LAN_SSID
    private val guestLanBssids = BuildConfig.WIFI_GUEST_LAN_BSSIDS.split(",")
    private val guestLanSecurity = WifiSecurity.WPA2
    private val guestLanPassword = BuildConfig.WIFI_GUEST_LAN_PASS

    @Inject
    lateinit var wifiRepository: WifiRepository

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun setup() {
        Dispatchers.setMain(mainThreadSurrogate)
        hiltRule.inject()
    }

    @After
    fun teardown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun testGetCurrentNetwork(): Unit = runBlocking {
        val network = wifiRepository.getCurrentNetwork().first()
        assert(network?.ssid ?: "" == lanSsid)
        assert(network?.bssid ?: "" in lanBssids)
//        assert(network?.security ?: WifiSecurity.UNKNOWN == lanSecurity)
    }

    @Test
    fun testNearbyNetworks(): Unit = runBlocking {
        val networks = wifiRepository.nearbyNetworks.first()
        assert(networks.isNotEmpty())

        val network = networks.first { it.ssid == lanSsid }
        assert(network.bssid in lanBssids)
//        assert(network.security == lanSecurity)
    }

    @Test
    fun testTryConnect() = runBlocking {
        val result = wifiRepository.tryConnect(
            ssid = guestLanSsid,
            bssid = guestLanBssids.first(),
            password = guestLanPassword,
            security = guestLanSecurity
        ).first()
        assert(result is NetworkEvent.Available)
    }
}