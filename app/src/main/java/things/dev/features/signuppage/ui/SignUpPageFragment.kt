package things.dev.features.signuppage.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import dagger.hilt.android.AndroidEntryPoint
import things.dev.R
import javax.inject.Inject

@AndroidEntryPoint
class SignUpPageFragment @Inject constructor() : Fragment() {
    private val viewModel: SignUpPageViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_up_page, container, false)
    }
}