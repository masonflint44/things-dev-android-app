package things.dev.features.signuppage.ui

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

@HiltViewModel
class SignUpPageViewModel @Inject constructor(): ViewModel() {
    val username: StateFlow<String> by lazy { MutableStateFlow("") }
    val password: StateFlow<String> by lazy { MutableStateFlow("") }
    val confirmPassword: StateFlow<String> by lazy { MutableStateFlow("") }

    fun onSignUpClicked() {
        TODO("not implemented")
    }
}