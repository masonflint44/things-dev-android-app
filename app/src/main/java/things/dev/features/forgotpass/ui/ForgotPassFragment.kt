package things.dev.features.forgotpass.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.coroutines.launch
import things.dev.R
import things.dev.databinding.FragmentForgotPassBinding
import javax.inject.Inject

class ForgotPassFragment @Inject constructor() : BottomSheetDialogFragment() {
    private val viewModel: ForgotPassViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentForgotPassBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_forgot_pass, container, false)
        binding.viewModel = viewModel

        // source: https://developer.android.com/topic/libraries/architecture/coroutines#restart
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch {
                    viewModel.toastMessage.collect {
                        Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
                    }
                }
                launch {
                    viewModel.dismiss.collect {
                        dismiss()
                    }
                }
            }
        }

        return binding.root
    }
}