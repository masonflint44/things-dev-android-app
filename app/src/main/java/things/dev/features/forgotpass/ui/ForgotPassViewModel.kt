package things.dev.features.forgotpass.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ForgotPassViewModel @Inject constructor(): ViewModel() {
    val email: StateFlow<String> by lazy { MutableStateFlow("") }

    private val _dismiss by lazy { Channel<Unit>() }
    val dismiss: Flow<Unit> by lazy { _dismiss.receiveAsFlow() }

    private val _toastMessage by lazy { Channel<String>() }
    val toastMessage: Flow<String> by lazy { _toastMessage.receiveAsFlow() }

    fun onResetPasswordClicked() {
        viewModelScope.launch {
            _toastMessage.send("Sending password reset email")
            _dismiss.send(Unit)
        }
    }
}