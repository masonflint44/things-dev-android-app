package things.dev.features.loginpage.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginPageViewModel @Inject constructor(): ViewModel() {
    val username: StateFlow<String> by lazy { MutableStateFlow("") }
    val password: StateFlow<String> by lazy { MutableStateFlow("") }

    private val _showForgotPassword by lazy { Channel<Unit>() }
    val showForgotPassword: Flow<Unit> by lazy { _showForgotPassword.receiveAsFlow() }

    fun onLoginWithGoogleClicked() {
        TODO("not implemented")
    }

    fun onLoginWithFacebookClicked() {
        TODO("not implemented")
    }

    fun onLoginWithUsernameClicked() {
        TODO("not implemented")
    }

    fun onSignUpClicked() {
        TODO("not implemented")
    }

    fun onForgotPasswordClicked() {
        viewModelScope.launch {
            _showForgotPassword.send(Unit)
        }
    }
}