package things.dev.features.loginpage.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import things.dev.R
import things.dev.databinding.LoginPageFragmentBinding
import things.dev.features.forgotpass.ui.ForgotPassFragment
import javax.inject.Inject

@AndroidEntryPoint
class LoginPageFragment @Inject constructor() : Fragment() {
    private val viewModel: LoginPageViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: LoginPageFragmentBinding = DataBindingUtil.inflate(inflater, R.layout.login_page_fragment, container, false)
        binding.viewModel = viewModel

        lifecycleScope.launch {
            viewModel.showForgotPassword
                .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
                .collect {
                    activity?.supportFragmentManager?.let {
                        val forgotPassFragment = ForgotPassFragment()
                        forgotPassFragment.show(it, "forgotPassFragment")
                    }
                }
        }

        return binding.root
    }
}