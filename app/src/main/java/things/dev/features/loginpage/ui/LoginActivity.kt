package things.dev.features.loginpage.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import dagger.hilt.android.AndroidEntryPoint
import things.dev.R
import things.dev.features.forgotpass.ui.ForgotPassViewModel
import things.dev.features.signuppage.ui.SignUpPageViewModel

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {
    private val loginPageViewModel: LoginPageViewModel by viewModels()
    private val forgotPassViewModel: ForgotPassViewModel by viewModels()
    private val signUpPageViewModel: SignUpPageViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }
}