package things.dev.features.wifiutils.framework.wificonfiguration

import android.net.wifi.WifiConfiguration
import things.dev.features.wifiutils.framework.models.WifiSecurity

class AndroidWifiConfigurationFactory: WifiConfigurationFactory {
    override fun createWifiConfiguration(
        ssid: String?,
        bssid: String?,
        password: String?,
        security: WifiSecurity?,
    ): WifiConfiguration {
        val wifiConfig = WifiConfiguration()
        ssid?.let {
            wifiConfig.SSID = "\"$it\""
        }
        bssid?.let {
            wifiConfig.BSSID = it
        }
        password?.let {
            when(security) {
                WifiSecurity.WEP -> {
                    wifiConfig.wepKeys[0] = "\"$password\""
                    wifiConfig.wepTxKeyIndex = 0
                    wifiConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE)
                    wifiConfig.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40)
                }
                WifiSecurity.WPA,
                WifiSecurity.WPA2,
                WifiSecurity.WPA3 -> wifiConfig.preSharedKey = "\"$password\""
                else -> {/* do nothing */}
            }
        }
        if (security == WifiSecurity.OPEN) {
            wifiConfig.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE)
        }
        return wifiConfig
    }
}