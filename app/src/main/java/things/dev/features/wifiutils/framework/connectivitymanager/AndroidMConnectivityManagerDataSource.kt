package things.dev.features.wifiutils.framework.connectivitymanager

import android.net.ConnectivityManager
import android.net.Network
import android.os.Build
import androidx.annotation.RequiresApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import things.dev.features.wifiutils.framework.models.NetworkEvent
import things.dev.features.wifiutils.framework.models.WifiNetwork
import things.dev.features.wifiutils.framework.models.WifiSecurity
import things.dev.features.wifiutils.framework.networkcallback.NetworkCallbackAdapter
import things.dev.features.wifiutils.framework.networkrequest.NetworkRequestFactory
import things.dev.features.wifiutils.framework.networkspecifier.NetworkSpecifierFactory
import javax.inject.Inject

class AndroidMConnectivityManagerDataSource @Inject constructor(
    private val networkSpecifierFactory: NetworkSpecifierFactory,
    private val networkRequestFactory: NetworkRequestFactory,
    private val networkCallbackAdapter: NetworkCallbackAdapter,
    private val connectivityManager: ConnectivityManager,
): ConnectivityManagerDataSource {
    override fun getCurrentNetwork(): Flow<WifiNetwork?> = flowOf(null)

    @RequiresApi(Build.VERSION_CODES.M)
    override fun tryConnect(
        ssid: String?,
        bssid: String?,
        password: String?,
        security: WifiSecurity?,
        hasInternet: Boolean,
        defaultOnClose: () -> Unit,
    ): Flow<NetworkEvent> {
        val networkSpecifier = networkSpecifierFactory.createNetworkSpecifier(ssid, bssid, password, security)
        val networkRequest = networkRequestFactory.createNetworkRequest(networkSpecifier)
        val defaultOnAvailable: (Network) -> Unit = if (!hasInternet) { it: Network -> bindProcessToNetwork(it) } else { _: Network -> }
        return networkCallbackAdapter.networkCallbackFlow(
            {
                connectivityManager.requestNetwork(
                    networkRequest,
                    it
                )
            },
            defaultOnAvailable,
            {
                connectivityManager.unregisterNetworkCallback(it)
                defaultOnClose.invoke()
            },
        )
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun bindProcessToNetwork(network: Network) = connectivityManager.bindProcessToNetwork(network)
}