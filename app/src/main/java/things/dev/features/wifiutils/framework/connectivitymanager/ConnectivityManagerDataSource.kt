package things.dev.features.wifiutils.framework.connectivitymanager

import android.net.Network
import kotlinx.coroutines.flow.Flow
import things.dev.features.wifiutils.framework.models.NetworkEvent
import things.dev.features.wifiutils.framework.models.WifiNetwork
import things.dev.features.wifiutils.framework.models.WifiSecurity

interface ConnectivityManagerDataSource {
    fun getCurrentNetwork(): Flow<WifiNetwork?>

    fun tryConnect(
        ssid: String?,
        bssid: String?,
        password: String?,
        security: WifiSecurity?,
        hasInternet: Boolean,
        defaultOnClose: () -> Unit = {},
    ): Flow<NetworkEvent>

    fun bindProcessToNetwork(network: Network): Boolean
}