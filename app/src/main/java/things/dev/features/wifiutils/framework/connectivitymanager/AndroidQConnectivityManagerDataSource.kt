package things.dev.features.wifiutils.framework.connectivitymanager

import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.net.wifi.WifiInfo
import android.os.Build
import androidx.annotation.RequiresApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.map
import things.dev.features.wifiutils.framework.models.NetworkEvent
import things.dev.features.wifiutils.framework.models.WifiNetwork
import things.dev.features.wifiutils.framework.models.WifiNetworkFactory
import things.dev.features.wifiutils.framework.models.WifiSecurity
import things.dev.features.wifiutils.framework.networkcallback.NetworkCallbackAdapter
import things.dev.features.wifiutils.framework.networkrequest.NetworkRequestFactory
import things.dev.features.wifiutils.framework.networkspecifier.NetworkSpecifierFactory
import javax.inject.Inject

class AndroidQConnectivityManagerDataSource @Inject constructor(
    private val networkSpecifierFactory: NetworkSpecifierFactory,
    private val networkRequestFactory: NetworkRequestFactory,
    private val networkCallbackAdapter: NetworkCallbackAdapter,
    private val wifiNetworkFactory: WifiNetworkFactory,
    private val connectivityManager: ConnectivityManager,
): ConnectivityManagerDataSource {
    private val ssidRegex = Regex("\"(.*)\"")

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun getCurrentNetwork() = networkCallbackAdapter.networkCallbackFlow({
        val networkRequest = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .build()
        connectivityManager.registerNetworkCallback(networkRequest, it)
    }, defaultOnClose = {
        connectivityManager.unregisterNetworkCallback(it)
    }).filter { it is NetworkEvent.CapabilitiesChanged }
        .map { (it as NetworkEvent.CapabilitiesChanged).networkCapabilities }
        .map { getWifiNetwork(it) }

    // TODO: update this to work with hidden ssids (see WifiInfo.getHiddenSSID)
    @RequiresApi(Build.VERSION_CODES.Q)
    private fun getWifiNetwork(capabilities: NetworkCapabilities): WifiNetwork? {
        val wifiInfo = when(val transportInfo = capabilities?.transportInfo) {
            is WifiInfo -> transportInfo
            else -> null
        }
        return when (wifiInfo) {
            null -> null
            // TODO: might be able to make AndroidSConnectivityManagerDataSource and use WifiInfo.getCurrentSecurityType to set security
            else -> wifiNetworkFactory.createWifiNetwork(
                getSsid(wifiInfo),
                wifiInfo.bssid,
                wifiInfo.frequency,
                wifiInfo.rssi,
            )
        }
    }

    private fun getSsid(wifiInfo: WifiInfo): String {
        return ssidRegex.matchEntire(wifiInfo.ssid)?.groupValues?.get(1) ?: wifiInfo.ssid
    }

    override fun tryConnect(
        ssid: String?,
        bssid: String?,
        password: String?,
        security: WifiSecurity?,
        hasInternet: Boolean,
        defaultOnClose: () -> Unit,
    ): Flow<NetworkEvent> {
        val networkSpecifier = networkSpecifierFactory.createNetworkSpecifier(ssid, bssid, password, security)
        val networkRequest = networkRequestFactory.createNetworkRequest(networkSpecifier)
        return networkCallbackAdapter.networkCallbackFlow(
            {
                connectivityManager.requestNetwork(
                    networkRequest,
                    it
                )
            },
            {},
            {
                connectivityManager.unregisterNetworkCallback(it)
                defaultOnClose.invoke()
            },
        )
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun bindProcessToNetwork(network: Network) = connectivityManager.bindProcessToNetwork(network)
}