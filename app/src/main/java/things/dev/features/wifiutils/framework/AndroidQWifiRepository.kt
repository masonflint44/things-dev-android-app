package things.dev.features.wifiutils.framework

import android.os.Build
import androidx.annotation.RequiresApi
import kotlinx.coroutines.flow.Flow
import things.dev.features.wifiutils.framework.connectivitymanager.ConnectivityManagerDataSource
import things.dev.features.wifiutils.framework.models.NetworkEvent
import things.dev.features.wifiutils.framework.models.WifiNetwork
import things.dev.features.wifiutils.framework.models.WifiSecurity
import things.dev.features.wifiutils.framework.wifimanager.WifiManagerDataSource
import javax.inject.Inject

class AndroidQWifiRepository @Inject constructor(
    private val connectivityManagerDataSource: ConnectivityManagerDataSource,
    wifiManagerDataSource: WifiManagerDataSource,
): WifiRepository {
    override val nearbyNetworks: Flow<List<WifiNetwork>> = wifiManagerDataSource.nearbyNetworks

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun getCurrentNetwork() = connectivityManagerDataSource.getCurrentNetwork()

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun tryConnect(
        ssid: String?,
        bssid: String?,
        password: String?,
        security: WifiSecurity?,
        hasInternet: Boolean
    ): Flow<NetworkEvent> {
        return connectivityManagerDataSource.tryConnect(ssid, bssid, password, security, hasInternet)
    }
}