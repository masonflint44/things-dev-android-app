package things.dev.features.wifiutils.framework.models

enum class WifiSecurity {
    WPA,
    WPA2,
    WPA3,
    WEP,
    OPEN,
    UNKNOWN,
}