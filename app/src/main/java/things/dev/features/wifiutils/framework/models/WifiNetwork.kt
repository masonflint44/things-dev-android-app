package things.dev.features.wifiutils.framework.models

data class WifiNetwork(
    val ssid: String,
    val bssid: String,
    val security: WifiSecurity,
    val freq: WifiFreq,
    val level: WifiLevel,
    val exactLevel: Int
)