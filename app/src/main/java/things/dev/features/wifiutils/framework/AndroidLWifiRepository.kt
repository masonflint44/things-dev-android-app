package things.dev.features.wifiutils.framework

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import things.dev.features.wifiutils.framework.connectivitymanager.ConnectivityManagerDataSource
import things.dev.features.wifiutils.framework.models.NetworkEvent
import things.dev.features.wifiutils.framework.models.WifiNetwork
import things.dev.features.wifiutils.framework.models.WifiSecurity
import things.dev.features.wifiutils.framework.wifimanager.WifiManagerDataSource
import javax.inject.Inject

class AndroidLWifiRepository @Inject constructor(
    private val connectivityManagerDataSource: ConnectivityManagerDataSource,
    private val wifiManagerDataSource: WifiManagerDataSource,
): WifiRepository {
    override val nearbyNetworks: Flow<List<WifiNetwork>> = wifiManagerDataSource.nearbyNetworks

    override fun getCurrentNetwork(): Flow<WifiNetwork?> = flowOf(wifiManagerDataSource.getCurrentNetwork())

    override fun tryConnect(
        ssid: String?,
        bssid: String?,
        password: String?,
        security: WifiSecurity?,
        hasInternet: Boolean,
    ): Flow<NetworkEvent> {
        val defaultOnClose: () -> Unit = if (!hasInternet) { -> wifiManagerDataSource.restoreDisabledNetworks() } else { -> }
        val networkEvents = connectivityManagerDataSource.tryConnect(ssid, bssid, password, security, hasInternet, defaultOnClose)
        wifiManagerDataSource.tryConnect(ssid, bssid, password, security, hasInternet)
        return networkEvents
    }
}