package things.dev.features.wifiutils.framework.di

import android.content.Context
import android.net.ConnectivityManager
import android.net.wifi.WifiManager
import android.os.Build
import androidx.lifecycle.LifecycleCoroutineScope
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import things.dev.features.wifiutils.framework.AndroidLWifiRepository
import things.dev.features.wifiutils.framework.AndroidQWifiRepository
import things.dev.features.wifiutils.framework.WifiRepository
import things.dev.features.wifiutils.framework.connectivitymanager.*
import things.dev.features.wifiutils.framework.models.WifiNetworkFactory
import things.dev.features.wifiutils.framework.networkcallback.AndroidLNetworkCallbackAdapter
import things.dev.features.wifiutils.framework.networkcallback.AndroidSNetworkCallbackAdapter
import things.dev.features.wifiutils.framework.networkcallback.NetworkCallbackAdapter
import things.dev.features.wifiutils.framework.networkrequest.AndroidLNetworkRequestFactory
import things.dev.features.wifiutils.framework.networkrequest.AndroidQNetworkRequestFactory
import things.dev.features.wifiutils.framework.networkrequest.NetworkRequestFactory
import things.dev.features.wifiutils.framework.networkspecifier.AndroidQNetworkSpecifierFactory
import things.dev.features.wifiutils.framework.networkspecifier.AndroidUnsuppotedNetworkSpecifierFactory
import things.dev.features.wifiutils.framework.networkspecifier.NetworkSpecifierFactory
import things.dev.features.wifiutils.framework.wificonfiguration.AndroidWifiConfigurationFactory
import things.dev.features.wifiutils.framework.wificonfiguration.WifiConfigurationFactory
import things.dev.features.wifiutils.framework.wifimanager.WifiManagerDataSource
import javax.inject.Named

@Module
@InstallIn(SingletonComponent::class)
abstract class WifiUtilsModule {
    companion object {
        @Provides
        fun provideConnectivityManagerDataSource(
            networkSpecifierFactory: NetworkSpecifierFactory,
            networkRequestFactory: NetworkRequestFactory,
            networkCallbackAdapter: NetworkCallbackAdapter,
            wifiNetworkFactory: WifiNetworkFactory,
            connectivityManager: ConnectivityManager,
        ): ConnectivityManagerDataSource {
            return when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> AndroidQConnectivityManagerDataSource(
                    networkSpecifierFactory,
                    networkRequestFactory,
                    networkCallbackAdapter,
                    wifiNetworkFactory,
                    connectivityManager
                )
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> AndroidMConnectivityManagerDataSource(
                    networkSpecifierFactory,
                    networkRequestFactory,
                    networkCallbackAdapter,
                    connectivityManager
                )
                else -> AndroidLConnectivityManagerDataSource(
                    networkSpecifierFactory,
                    networkRequestFactory,
                    networkCallbackAdapter,
                    connectivityManager
                )
            }
        }

        @Provides
        fun provideNetworkCallbackAdapter(
            externalScope: CoroutineScope,
            @Named("NetworkCallbackFlags") flags: Int
        ): NetworkCallbackAdapter {
            return when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> AndroidSNetworkCallbackAdapter(flags, externalScope)
                else -> AndroidLNetworkCallbackAdapter(externalScope)
            }
        }

        @Provides
        fun provideNetworkRequestFactory(): NetworkRequestFactory {
            return when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> AndroidQNetworkRequestFactory()
                else -> AndroidLNetworkRequestFactory()
            }
        }

        @Provides
        fun provideNetworkSpecifierFactory(): NetworkSpecifierFactory {
            return when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> AndroidQNetworkSpecifierFactory()
                else -> AndroidUnsuppotedNetworkSpecifierFactory()
            }
        }

        @Provides
        fun provideWifiConfigurationFactory(): WifiConfigurationFactory = AndroidWifiConfigurationFactory()

        @Provides
        fun provideWifiManagerDataSource(
            wifiManager: WifiManager,
            wifiConfigurationFactory: WifiConfigurationFactory,
            @ApplicationContext applicationContext: Context,
            wifiNetworkFactory: WifiNetworkFactory,
            externalScope: CoroutineScope,
        ): WifiManagerDataSource {
            return WifiManagerDataSource(wifiManager, wifiConfigurationFactory, applicationContext, wifiNetworkFactory, externalScope)
        }

        @Provides
        fun provideWifiRepository(
            connectivityManagerDataSource: ConnectivityManagerDataSource,
            wifiManagerDataSource: WifiManagerDataSource,
        ): WifiRepository {
            return when {
                Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q -> AndroidQWifiRepository(connectivityManagerDataSource, wifiManagerDataSource)
                else -> AndroidLWifiRepository(connectivityManagerDataSource, wifiManagerDataSource)
            }
        }

        @Provides
        @Named("NetworkCallbackFlags")
        fun providesNetworkCallbackFlags() = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
            ConnectivityManager.NetworkCallback.FLAG_INCLUDE_LOCATION_INFO
            else 0

        @Provides
        fun providesConnectivityManager(@ApplicationContext applicationContext: Context): ConnectivityManager {
            return applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        }

        @Provides
        fun providesWifiManager(@ApplicationContext applicationContext: Context): WifiManager {
            return applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        }

        @Provides
        fun providesCoroutineScope(): CoroutineScope {
            return MainScope()
        }
    }
}