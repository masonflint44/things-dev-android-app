package things.dev.features.wifiutils.framework.wifimanager

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import androidx.core.app.ActivityCompat
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.shareIn
import things.dev.features.wifiutils.framework.models.WifiNetwork
import things.dev.features.wifiutils.framework.models.WifiNetworkFactory
import things.dev.features.wifiutils.framework.models.WifiSecurity
import things.dev.features.wifiutils.framework.wificonfiguration.WifiConfigurationFactory
import javax.inject.Inject

class WifiManagerDataSource @Inject constructor(
    private val wifiManager: WifiManager,
    private val wifiConfigurationFactory: WifiConfigurationFactory,
    @ApplicationContext private val applicationContext: Context,
    private val wifiNetworkFactory: WifiNetworkFactory,
    externalScope: CoroutineScope,
) {
    private val disabledNetworkBssids = mutableListOf<String>()

    val nearbyNetworks: Flow<List<WifiNetwork>> = callbackFlow {
        val scanResultsReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context?, intent: Intent?) {
//                Log.d(tag,"Completed wifi network scan")
                val scanResults = wifiManager.scanResults.map {
                    wifiNetworkFactory.createWifiNetwork(it.SSID, it.BSSID, it.frequency, it.level, it.capabilities)
                }
                trySend(scanResults)
            }
        }

//        Log.d(tag,"Starting wifi network scan")
        applicationContext.registerReceiver(scanResultsReceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))
        wifiManager.startScan()

        awaitClose { applicationContext.unregisterReceiver(scanResultsReceiver) }
    }.shareIn(externalScope, SharingStarted.Lazily, 1)

    fun getCurrentNetwork(): WifiNetwork? {
        return wifiNetworkFactory.createWifiNetwork(
            wifiManager.connectionInfo.ssid,
            wifiManager.connectionInfo.bssid,
            wifiManager.connectionInfo.frequency,
            wifiManager.connectionInfo.rssi
        )
    }

    fun restoreDisabledNetworks() {
        if (ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) ==
            PackageManager.PERMISSION_DENIED) {
            return
        }
        for (config in wifiManager.configuredNetworks) {
            if (!disabledNetworkBssids.contains(config.BSSID)) {
                continue
            }
            wifiManager.enableNetwork(config.networkId, false)
        }
        disabledNetworkBssids.clear()
    }

    fun tryConnect(
        ssid: String?,
        bssid: String?,
        password: String?,
        security: WifiSecurity?,
        hasInternet: Boolean,
    ) {
        val wifiConfig = wifiConfigurationFactory.createWifiConfiguration(ssid, bssid, password, security)
        val networkId = wifiManager.addNetwork(wifiConfig)
        wifiManager.disconnect()

        if (!hasInternet &&
            ActivityCompat.checkSelfPermission(applicationContext, Manifest.permission.ACCESS_FINE_LOCATION) ==
            PackageManager.PERMISSION_GRANTED) {
            for (config in wifiManager.configuredNetworks) {
                if (config.status == WifiConfiguration.Status.DISABLED) { continue }
                wifiManager.disableNetwork(config.networkId)
                if (config.BSSID == null) { continue }
                disabledNetworkBssids.add(config.BSSID)
            }
        }
        wifiManager.enableNetwork(networkId, true)
        wifiManager.reconnect()
    }
}