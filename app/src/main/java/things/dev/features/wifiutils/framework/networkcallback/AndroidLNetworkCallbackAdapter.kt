package things.dev.features.wifiutils.framework.networkcallback

import android.net.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.shareIn
import things.dev.features.wifiutils.framework.connectivitymanager.ConnectivityManagerDataSource
import things.dev.features.wifiutils.framework.models.NetworkEvent
import javax.inject.Inject

class AndroidLNetworkCallbackAdapter @Inject constructor(
    private val externalScope: CoroutineScope,
): NetworkCallbackAdapter {
    override fun networkCallbackFlow(
        callback: (networkCallback: ConnectivityManager.NetworkCallback) -> Unit,
        defaultOnAvailable: (network: Network) -> Unit,
        defaultOnClose: (networkCallback: ConnectivityManager.NetworkCallback) -> Unit,
    ): Flow<NetworkEvent> = callbackFlow {
        val networkCallback = object: ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                super.onAvailable(network)
                defaultOnAvailable(network)
                trySend(NetworkEvent.Available(network))
            }

            override fun onBlockedStatusChanged(network: Network, blocked: Boolean) {
                super.onBlockedStatusChanged(network, blocked)
                trySend(NetworkEvent.BlockedStatusChanged(network, blocked))
            }

            override fun onCapabilitiesChanged(
                network: Network,
                networkCapabilities: NetworkCapabilities
            ) {
                super.onCapabilitiesChanged(network, networkCapabilities)
                trySend(NetworkEvent.CapabilitiesChanged(network, networkCapabilities))
            }

            override fun onLinkPropertiesChanged(network: Network, linkProperties: LinkProperties) {
                super.onLinkPropertiesChanged(network, linkProperties)
                trySend(NetworkEvent.LinkPropertiesChanged(network, linkProperties))
            }

            override fun onLosing(network: Network, maxMsToLive: Int) {
                super.onLosing(network, maxMsToLive)
                trySend(NetworkEvent.Losing(network, maxMsToLive))
            }

            override fun onLost(network: Network) {
                super.onLost(network)
                trySend(NetworkEvent.Lost(network))
            }

            override fun onUnavailable() {
                super.onUnavailable()
                trySend(NetworkEvent.Unavailable)
            }
        }

        callback(networkCallback)

        awaitClose {
            defaultOnClose(networkCallback)
        }
    }.shareIn(externalScope, SharingStarted.WhileSubscribed(), 1)
}