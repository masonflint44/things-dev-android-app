package things.dev.features.wifiutils.framework

import kotlinx.coroutines.flow.Flow
import things.dev.features.wifiutils.framework.models.NetworkEvent
import things.dev.features.wifiutils.framework.models.WifiNetwork
import things.dev.features.wifiutils.framework.models.WifiSecurity

interface WifiRepository {
    val nearbyNetworks: Flow<List<WifiNetwork>>

    fun getCurrentNetwork(): Flow<WifiNetwork?>

    fun tryConnect(
        ssid: String? = null,
        bssid: String? = null,
        password: String? = null,
        security: WifiSecurity? = null,
        hasInternet: Boolean = true,
    ): Flow<NetworkEvent>
}