package things.dev.features.wifiutils.framework.models

enum class WifiFreq {
    FREQ_2_4_GHZ,
    FREQ_5_GHZ,
    OTHER
}