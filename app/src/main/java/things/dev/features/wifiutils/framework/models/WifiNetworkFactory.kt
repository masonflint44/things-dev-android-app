package things.dev.features.wifiutils.framework.models

import javax.inject.Inject

class WifiNetworkFactory @Inject constructor() {
    fun createWifiNetwork(
        ssid: String,
        bssid: String,
        exactFreq: Int,
        exactLevel: Int,
        capabilities: String
    ): WifiNetwork {
        val freq = getWifiFreq(exactFreq)
        val level = getWifiLevel(exactLevel)
        val security = getWifiSecurity(capabilities)
        return WifiNetwork(
            ssid = ssid,
            bssid = bssid,
            security = security,
            freq = freq,
            level = level,
            exactLevel = exactLevel,
        )
    }

    fun createWifiNetwork(
        ssid: String,
        bssid: String,
        exactFreq: Int,
        exactLevel: Int,
        security: WifiSecurity = WifiSecurity.UNKNOWN
    ): WifiNetwork {
        val freq = getWifiFreq(exactFreq)
        val level = getWifiLevel(exactLevel)
        return WifiNetwork(
            ssid = ssid,
            bssid = bssid,
            security = security,
            freq = freq,
            level = level,
            exactLevel = exactLevel,
        )
    }

    private fun getWifiSecurity(capabilities: String?): WifiSecurity {
        return when {
            capabilities == null -> WifiSecurity.UNKNOWN
            capabilities.contains("WPA3") -> WifiSecurity.WPA3
            capabilities.contains("WPA2") -> WifiSecurity.WPA2
            capabilities.contains("WPA") -> WifiSecurity.WPA
            capabilities.contains("WEP") -> WifiSecurity.WEP
            else -> WifiSecurity.OPEN
        }
    }

    private fun getWifiFreq(freq: Int): WifiFreq {
        return when {
            freq >= 5000 -> WifiFreq.FREQ_5_GHZ
            freq >= 2400 -> WifiFreq.FREQ_2_4_GHZ
            else -> WifiFreq.OTHER
        }
    }

    private fun getWifiLevel(level: Int): WifiLevel {
        return when {
            level > -44 -> WifiLevel.EXCELLENT
            level > -55 -> WifiLevel.GOOD
            level > -72 -> WifiLevel.FAIR
            level > -86 -> WifiLevel.WEAK
            else -> WifiLevel.NONE
        }
    }
}