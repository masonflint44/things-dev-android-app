package things.dev.features.wifiutils.framework.models

enum class WifiLevel {
    NONE,
    WEAK,
    FAIR,
    GOOD,
    EXCELLENT,
    UNKNOWN
}