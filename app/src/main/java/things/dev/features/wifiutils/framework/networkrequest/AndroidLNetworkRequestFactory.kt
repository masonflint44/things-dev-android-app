package things.dev.features.wifiutils.framework.networkrequest

import android.net.NetworkCapabilities
import android.net.NetworkRequest
import things.dev.features.wifiutils.framework.networkspecifier.NetworkSpecifierWrapper

class AndroidLNetworkRequestFactory: NetworkRequestFactory {
    override fun createNetworkRequest(
        networkSpecifier: NetworkSpecifierWrapper?,
    ): NetworkRequest {
        val builder = NetworkRequest.Builder()
        builder.addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        return builder.build()
    }
}