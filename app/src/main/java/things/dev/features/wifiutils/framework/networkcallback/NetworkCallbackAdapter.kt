package things.dev.features.wifiutils.framework.networkcallback

import android.net.ConnectivityManager
import android.net.Network
import kotlinx.coroutines.flow.Flow
import things.dev.features.wifiutils.framework.models.NetworkEvent

interface NetworkCallbackAdapter {
    fun networkCallbackFlow(
        callback: (networkCallback: ConnectivityManager.NetworkCallback) -> Unit,
        defaultOnAvailable: (network: Network) -> Unit = {},
        defaultOnClose: (networkCallback: ConnectivityManager.NetworkCallback) -> Unit = {},
    ): Flow<NetworkEvent>
}