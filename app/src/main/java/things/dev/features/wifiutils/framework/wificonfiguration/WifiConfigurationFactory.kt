package things.dev.features.wifiutils.framework.wificonfiguration

import android.net.wifi.WifiConfiguration
import things.dev.features.wifiutils.framework.models.WifiSecurity

interface WifiConfigurationFactory {
    fun createWifiConfiguration(
        ssid: String? = null,
        bssid: String? = null,
        password: String? = null,
        security: WifiSecurity? = null,
    ): WifiConfiguration
}