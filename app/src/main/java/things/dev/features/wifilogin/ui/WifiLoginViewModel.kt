package things.dev.features.wifilogin.ui

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import things.dev.features.wifiutils.framework.models.WifiNetwork

class WifiLoginViewModel : ViewModel() {
    val scanResult: MutableLiveData<WifiNetwork> by lazy {
        MutableLiveData<WifiNetwork>()
    }
    val password: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
    val loading: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }
    val fabClicked: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }
    val isVisible: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }
    val nextPage: MutableLiveData<Boolean> by lazy {
        MutableLiveData<Boolean>()
    }
    val networkConnected: MutableLiveData<WifiNetwork> by lazy {
        MutableLiveData<WifiNetwork>()
    }
}